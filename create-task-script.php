<?php

$taskName = $_POST['name'];
$taskDescription = $_POST['description'];

require "connection.php";

$insertStatement = $conn->prepare('INSERT INTO `tasks`(name, description) VALUES (:name, :description)');
$insertStatement->execute([
    'name' => $taskName,
    'description' => $taskDescription
]);

header('Location: index.php');
