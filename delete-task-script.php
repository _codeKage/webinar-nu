<?php

require "connection.php";

$taskId = $_POST['taskId'];

$deleteStatement = $conn->prepare('DELETE FROM `tasks` WHERE id = :id');
$deleteStatement->execute([
    'id' => $taskId
]);
