<?php
require "connection.php";

$taskId = $_POST['taskId'];

echo $taskId;

$updateStatement = $conn->prepare('UPDATE `tasks` SET `done` = 1 WHERE id = :id');
$updateStatement->execute([
    'id' => $taskId
]);

header('Location: index.php');
