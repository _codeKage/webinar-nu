<?php
require "connection.php";
$selectStatement = $conn->prepare('SELECT * FROM `tasks`');
$selectStatement->execute();
?>

<!DOCTYPE html>
<html>

<head>
    <title>To do list</title>
    <?php
    include "css.php";
    ?>
</head>

<body>
    <table class="table table-striped">
        <tr>
            <th>Task Id</th>
            <th>Task Name</th>
            <th>Task Description</th>
            <th>Is Done</th>
            <th></th>
        </tr>
        <?php
        $tasks = $selectStatement->fetchAll();
        foreach ($tasks as $task) {
        ?>
            <tr>
                <td>
                    <?= $task['id'] ?>
                </td>
                <td>
                    <?= $task['name'] ?>
                </td>
                <td>
                    <?= $task['description'] ?>
                </td>
                <td>
                    <?php
                    if (!$task['done']) {
                    ?>
                        <form action="update-task-script.php" method="POST">
                            <input type="hidden" value="<?= $task['id'] ?>" name="taskId">
                            <input type="submit" value="Done" class="btn btn-primary">
                        </form>
                    <?php
                    } else {
                    ?>
                        <span>Task is already done.</span>
                    <?php
                    }
                    ?>
                </td>
                <td>
                    <button class="btn btn-danger" onclick="deleteTask(<?= $task['id'] ?>)" value="<?= $task['id'] ?>" id="delete-button">Delete</button>
                </td>
            </tr>
        <?php
        }
        ?>
    </table>
    <a href="create-task.php" class="btn btn-success"> Create Task</a>

    <script>
        function deleteTask(taskId) {
            console.log(taskId);
            $.ajax({
                type: 'POST',
                data: {
                    taskId
                },
                url: 'delete-task-script.php',
                success: function(d) {

                }
            })
        }
    </script>
    <form action="upload-file.php" method="POST" enctype="multipart/form-data">
        <input type="file" name="fileToUpload">
        <input type="submit" value="submit" class="btn btn-primary">
    </form>

    <?php
    $imageSelect = $conn->prepare('SELECT * FROM `uploads`');
    $imageSelect->execute();
    $images = $imageSelect->fetchAll();
    foreach ($images as $image) {
    ?>
        <img src="<?= $image['location'] ?>">
    <?php
    }
    ?>


</body>

</html>