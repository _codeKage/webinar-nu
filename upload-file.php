<?php
$targetDirectory = 'uploads/';
$target_file = $targetDirectory . basename($_FILES["fileToUpload"]["name"]);

$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

if ($imageFileType !== 'png' && $imageFileType !== 'jpg') {
    echo $imageFileType;
    echo 'invalid image type';
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        require "connection.php";
        $insertUpload = $conn->prepare('INSERT INTO `uploads`(location) VALUES (:location)');
        $insertUpload->execute([
            'location' => $target_file
        ]);
        header('Location: index.php');
    } else {
        echo 'error';
    }
}
