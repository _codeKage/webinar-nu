<!DOCTYPE html>
<html>

<head>
    <title>Create Task</title>
    <?php
    include "css.php";
    ?>
</head>

<body>
    <div>
        <div>
            <form action="create-task-script.php" method="POST">
                <div class="form-group">
                    <label for="name">Name: </label>
                    <input type="text" class="form-control" name="name" required>
                </div>
                <div class="form-group">
                    <label for="name">Description: </label>
                    <input type="text" class="form-control" name="description" required>
                </div>
                <div class="form-group">
                    <input type="submit" name="submit" value="Create Task" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</body>

</html>