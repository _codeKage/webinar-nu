<?php

try {
    $conn = new PDO('mysql:host=localhost;dbname=sample-project', 'root', 'root', array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ));
} catch (PDOException $e) {
    echo 'error';
}
